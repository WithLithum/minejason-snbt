# MineJason SNBT

MineJason SNBT is a library that provides essential support for dealing with String NBT in Minecraft: Java Edition.

This is not an extension module, but a base module that (eventually) will be required by MineJason.

## Building

You will need .NET 8. Open your favourite IDE and build the solution, or use your favourite shell, navigate to the repository and enter `dotnet build`.
