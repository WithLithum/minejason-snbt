﻿using MineJason.SNbt.Values;
using Moq;

namespace MineJason.SNbt.Tests;

public class WriterTests
{
    [Fact]
    public void WriteValue_PlainValue()
    {
        var mock = new Mock<ISNbtValue>();
        
        var str = new StringWriter();
        var writer = new SNbtWriter(str);
        writer.WriteValue(mock.Object);
        
        writer.Dispose();
        mock.Verify(x => x.ToSNbtString(), Times.Once());
    }
    
    [Fact]
    public void WriteComma()
    {
        var str = new StringWriter();
        var writer = new SNbtWriter(str);
        
        writer.WriteComma();
        writer.WriteComma();
        
        Assert.Equal(",",
            str.ToString());
    }
    
    [Fact]
    public void WriteStringProperty_SingleQuote_NoEscape()
    {
        var str = new StringWriter();
        var writer = new SNbtWriter(str);
        
        writer.WriteProperty("TestProperty", "AString", true);
        writer.Dispose();

        Assert.Equal("TestProperty:'AString'",
            str.ToString());
    }
    
    [Fact]
    public void WriteStringProperty_SingleQuote_DoEscape()
    {
        var str = new StringWriter();
        var writer = new SNbtWriter(str);
        
        writer.WriteProperty("TestProperty", "AStringEscapeMe'Me", true);
        writer.Dispose();

        Assert.Equal("TestProperty:'AStringEscapeMe\\'Me'",
            str.ToString());
    }
    
    [Fact]
    public void WriteStringProperty_SingleQuote_EscapeSlash()
    {
        var str = new StringWriter();
        var writer = new SNbtWriter(str);
        
        writer.WriteProperty("TestString", "IAmSlash\\EscapeMe", true);
        writer.Dispose();

        Assert.Equal(@"TestString:'IAmSlash\\EscapeMe'",
            str.ToString());
    }
    
    [Fact]
    public void WriteStringProperty_DoubleQuote_NoEscape()
    {
        var str = new StringWriter();
        var writer = new SNbtWriter(str);
        
        writer.WriteProperty("TestString", "AnotherStringHere");
        writer.Dispose();

        Assert.Equal("TestString:\"AnotherStringHere\"",
            str.ToString());
    }
    
    [Fact]
    public void WriteStringProperty_DoubleQuote_DoEscape()
    {
        var str = new StringWriter();
        var writer = new SNbtWriter(str);
        
        writer.WriteProperty("TestString", "AnotherEscaped\"String\"Here");
        writer.Dispose();

        Assert.Equal("TestString:\"AnotherEscaped\\\"String\\\"Here\"",
            str.ToString());
    }
    
    [Fact]
    public void WriteStringProperty_DoubleQuote_EscapeSlash()
    {
        var str = new StringWriter();
        var writer = new SNbtWriter(str);
        
        writer.WriteProperty("TestString", "IAmSlash\\EscapeMe");
        writer.Dispose();

        Assert.Equal("TestString:\"IAmSlash\\\\EscapeMe\"",
            str.ToString());
    }
    
    [Fact]
    public void NestedCompound()
    {
        var str = new StringWriter();
        var writer = new SNbtWriter(str);
        
        writer.WriteBeginCompound();
        writer.WriteProperty("Test", 123);
        writer.WritePropertyCompoundBegin("Compound");
        writer.WriteProperty("Number", 456);
        writer.WriteProperty("String", "My String");
        writer.WriteEndCompound();
        writer.WriteEndCompound();
        writer.Dispose();

        Assert.Equal("{Test:123,Compound:{Number:456,String:\"My String\"}}",
            str.ToString());
    }
}