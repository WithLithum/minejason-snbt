﻿using System.Globalization;
using JetBrains.Annotations;
using MineJason.SNbt.Values;

namespace MineJason.SNbt;

public sealed class SNbtWriter : IDisposable
{
    public SNbtWriter(TextWriter target)
    {
        _target = target;
    }
    
    private readonly TextWriter _target;
    private bool _writeComma;

    /// <summary>
    /// Writes a comma. The first call to this method is ignored.
    /// </summary>
    [PublicAPI]
    public void WriteComma()
    {
        if (!_writeComma)
        {
            _writeComma = true;
            return;
        }
        
        _target.Write(',');
    }

    /// <summary>
    /// Writes the beginning of a compound and resets the comma status.
    /// </summary>
    [PublicAPI]
    public void WriteBeginCompound()
    {
        _target.Write('{');
        _writeComma = false;
    }

    /// <summary>
    /// Writes the end of a compound. Subsequent calls to <see cref="WriteComma"/> writes a comma.
    /// </summary>
    [PublicAPI]
    public void WriteEndCompound()
    {
        _target.Write('}');
        _writeComma = true;
    }

    /// <summary>
    /// Writes the beginning of an array.
    /// </summary>
    /// <param name="specifier">The specifier.</param>
    public void WriteBeginArray(char specifier)
    {
        _target.Write('[');
        _target.Write(specifier);
        _target.Write(';');
        _writeComma = false;
    }

    /// <summary>
    /// Writes the beginning of a list and resets the comma status.
    /// </summary>
    public void WriteBeginList()
    {
        _target.Write('[');
        _writeComma = false;
    }

    /// <summary>
    /// Writes the end of a compound. Subsequent calls to <see cref="WriteComma"/> writes a comma.
    /// </summary>
    public void WriteEndList()
    {
        _target.Write(']');
        _writeComma = true;
    }

    /// <summary>
    /// Writes a key name and a semicolon as the beginning of a property. A comma is automatically added
    /// before the key name if required.
    /// </summary>
    /// <param name="key">The key name.</param>
    public void WriteKeyName(string key)
    {
        WriteComma();
        _target.Write(key);
        _target.Write(':');
    }

    /// <summary>
    /// Writes a string value.
    /// </summary>
    /// <param name="value">The value to write.</param>
    /// <param name="singleQuote">If <see langword="true"/>, uses single quote.</param>
    [PublicAPI]
    public void WriteValue(ReadOnlySpan<char> value, bool singleQuote = false)
    {
        var quote = singleQuote ? '\'' : '"';
        var escapedForm = singleQuote ? "\\'" : "\\\"";
        
        _target.Write(quote);
        foreach (var x in value)
        {
            if (x == '\\')
            {
                _target.Write(@"\\");
                continue;
            }

            if (x == quote)
            {
                _target.Write(escapedForm);
                continue;
            }
            
            _target.Write(x);
        }
        _target.Write(quote);
    }

    /// <summary>
    /// Writes the invariable culture format value.
    /// </summary>
    /// <param name="value">The value to write.</param>
    [PublicAPI]
    public void WriteValue(IFormattable value)
    {
        _target.Write(value.ToString(null, CultureInfo.InvariantCulture));
    }
    
    /// <summary>
    /// Writes the invariable culture format value.
    /// </summary>
    /// <param name="value">The value to write.</param>
    /// <param name="suffix">The suffix of the value.</param>
    [PublicAPI]
    public void WriteValue(IFormattable value, char suffix)
    {
        WriteValue(value);
        _target.Write(suffix);
    }

    public void WriteValue(ISNbtWritable value)
    {
        value.WriteTo(this);
    }

    public void WriteValue(ISNbtValue value)
    {
        _target.Write(value.ToSNbtString());
    }
    
    /// <summary>
    /// Writes a string property.
    /// </summary>
    /// <param name="key">The name of the key.</param>
    /// <param name="value">The value of the key.</param>
    /// <param name="singleQuote">If <see langword="true"/>, uses single quote.</param>
    [PublicAPI]
    public void WriteProperty(string key, ReadOnlySpan<char> value, bool singleQuote = false)
    {
        WriteKeyName(key);
        WriteValue(value, singleQuote);
    }

    /// <summary>
    /// Writes the specified property.
    /// </summary>
    /// <param name="key">The name of the key.</param>
    /// <param name="value">The value of the key.</param>
    [PublicAPI]
    public void WriteProperty(string key, ISNbtValue value)
    {
        WriteKeyName(key);
        WriteValue(value);
    }

    /// <summary>
    /// Writes the specified property.
    /// </summary>
    /// <param name="key">The name of the key.</param>
    /// <param name="value">The value of the key.</param>
    [PublicAPI]
    public void WriteProperty(string key, ISNbtWritable value)
    {
        WriteKeyName(key);
        WriteValue(value);
    }

    /// <summary>
    /// Writes the specified property value.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    [PublicAPI]
    public void WriteProperty(string key, IFormattable value)
    {
        WriteKeyName(key);
        WriteValue(value);
    }

    [PublicAPI]
    public void WritePropertyCompoundBegin(string key)
    {
        WriteKeyName(key);
        WriteBeginCompound();
    }
    
    #region Disposing

    /// <summary>
    /// Disposes the writer held by this instance.
    /// </summary>
    public void Dispose()
    {
        Dispose(true);
    }
    
    /// <summary>
    /// Disposes the writer held by this instance.
    /// </summary>
    /// <param name="disposing">If <see langword="true"/>, disposes the writer.</param>
    [PublicAPI]
    public void Dispose(bool disposing)
    {
        if (disposing)
        {
            _target.Dispose();
        }
    }
    
    #endregion
}