﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;
using JetBrains.Annotations;

namespace MineJason.SNbt.Values;

public class SNbtByteArray : Collection<sbyte>, ISNbtValue, ISNbtWritable
{
    /// <summary>
    /// Adds the specified value to this instance.
    /// </summary>
    /// <param name="value">The value.</param>
    [PublicAPI]
    public void Add(SNbtByteValue value)
    {
        base.Add(value.Value);
    }

    public string ToSNbtString()
    {
        return SNbtArrayHelper.ToSNbtString('B', this);
    }

    public void WriteTo(SNbtWriter writer)
    {
        SNbtArrayHelper.WriteArray('B', this, writer);
    }
}