﻿namespace MineJason.SNbt.Values;

/// <summary>
/// Represents a String NBT value.
/// </summary>
public interface ISNbtValue
{
    /// <summary>
    /// Converts this instance to its String NBT representation.
    /// </summary>
    /// <returns>The String NBT representation.</returns>
    string ToSNbtString();
}