﻿namespace MineJason.SNbt.Values;

public interface ISNbtWritable
{
    void WriteTo(SNbtWriter writer);
}