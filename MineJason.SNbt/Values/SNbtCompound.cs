﻿using System.Text;

namespace MineJason.SNbt.Values;

public sealed class SNbtCompound : Dictionary<string, ISNbtValue>, ISNbtValue, ISNbtWritable
{
    public void WriteTo(SNbtWriter writer)
    {
        writer.WriteBeginCompound();

        foreach (var value in this)
        {
            if (value.Value is ISNbtWritable writable)
            {
                writer.WriteProperty(value.Key, writable);
                continue;
            }
            
            writer.WriteProperty(value.Key, value.Value);
        }
        
        writer.WriteEndCompound();
    }
    
    public string ToSNbtString()
    {
        var stream = new StringWriter();
        using (var writer = new SNbtWriter(stream))
        {
            WriteTo(writer);
        }

        return stream.ToString();
    }
}