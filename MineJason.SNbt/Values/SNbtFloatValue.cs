﻿namespace MineJason.SNbt.Values;

using System.Globalization;
using JetBrains.Annotations;

[PublicAPI]
public record struct SNbtFloatValue : ISNbtValue, ISNbtWritable
{
    /// <summary>
    /// The suffix of the NBT representation of this value.
    /// </summary>
    public const char ValueSuffix = 'f';
    
    public SNbtFloatValue(float value)
    {
        Value = value;
    }
    
    public float Value { get; set; }
    
    public string ToSNbtString()
    {
        return $"{Value.ToString(CultureInfo.InvariantCulture)}{ValueSuffix}";
    }

    public void WriteTo(SNbtWriter writer)
    {
        writer.WriteValue(Value, ValueSuffix);
    }
    
    /// <summary>
    /// Converts the specified value to its string NBT value form.
    /// </summary>
    /// <param name="from">The value to convert from.</param>
    /// <returns>The string NBT value form.</returns>
    public static implicit operator SNbtFloatValue(float from)
    {
        return new SNbtFloatValue(from);
    }

    /// <summary>
    /// Converts the specified string NBT form of value to its value representation.
    /// </summary>
    /// <param name="from">The string NBT value to convert from.</param>
    /// <returns>The value form.</returns>
    public static implicit operator float(SNbtFloatValue from)
    {
        return from.Value;
    }
}