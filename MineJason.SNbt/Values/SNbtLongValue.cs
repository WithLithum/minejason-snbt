﻿namespace MineJason.SNbt.Values;

using System.Globalization;
using JetBrains.Annotations;

[PublicAPI]
public record struct SNbtLongValue : ISNbtValue, ISNbtWritable
{
    /// <summary>
    /// The suffix of the NBT representation of this value.
    /// </summary>
    public const char ValueSuffix = 'L';
    
    public SNbtLongValue(long value)
    {
        Value = value;
    }
    
    public long Value { get; set; }
    
    public string ToSNbtString()
    {
        return $"{Value.ToString(CultureInfo.InvariantCulture)}{ValueSuffix}";
    }

    public void WriteTo(SNbtWriter writer)
    {
        writer.WriteValue(Value, ValueSuffix);
    }
    
    /// <summary>
    /// Converts the specified value to its string NBT value form.
    /// </summary>
    /// <param name="from">The value to convert from.</param>
    /// <returns>The string NBT value form.</returns>
    public static implicit operator SNbtLongValue(long from)
    {
        return new SNbtLongValue(from);
    }

    /// <summary>
    /// Converts the specified string NBT form of value to its value representation.
    /// </summary>
    /// <param name="from">The string NBT value to convert from.</param>
    /// <returns>The value form.</returns>
    public static implicit operator long(SNbtLongValue from)
    {
        return from.Value;
    }
}