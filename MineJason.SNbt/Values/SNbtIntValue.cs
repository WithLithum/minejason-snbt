﻿namespace MineJason.SNbt.Values;

using System.Globalization;
using JetBrains.Annotations;

[PublicAPI]
public record struct SNbtIntValue : ISNbtValue, ISNbtWritable
{
    public SNbtIntValue(int value)
    {
        Value = value;
    }
    
    public int Value { get; set; }
    
    public string ToSNbtString()
    {
        return Value.ToString(CultureInfo.InvariantCulture);
    }

    public static implicit operator SNbtIntValue(int from)
    {
        return new SNbtIntValue(from);
    }

    public static implicit operator int(SNbtIntValue from)
    {
        return from.Value;
    }

    public void WriteTo(SNbtWriter writer)
    {
        writer.WriteValue(Value);
    }
}