﻿namespace MineJason.SNbt.Values;

using System.Globalization;
using JetBrains.Annotations;

/// <summary>
/// Represents a String NBT value consisting of a <see cref="sbyte"/>.
/// </summary>
public record struct SNbtByteValue : ISNbtValue, ISNbtWritable
{
    /// <summary>
    /// The suffix of the NBT representation of this value.
    /// </summary>
    public const char ValueSuffix = 'b';
    
    [PublicAPI]
    public SNbtByteValue(bool value)
    {
        Value = value ? (sbyte)1 : (sbyte)0;
    }

    [PublicAPI]
    public SNbtByteValue(sbyte value)
    {
        Value = value;
    }
    
    [PublicAPI]
    public sbyte Value { get; set; }
    
    public string ToSNbtString()
    {
        return $"{Value.ToString(CultureInfo.InvariantCulture)}{ValueSuffix}";
    }

    /// <summary>
    /// Returns the boolean value of this instance.
    /// </summary>
    /// <returns><see langword="true"/> if <see cref="Value"/> equals to or is bigger than <c>1</c>; otherwise, <see langword="false"/>.</returns>
    [PublicAPI]
    public bool BooleanValue()
    {
        return Value >= 1;
    }

    public void WriteTo(SNbtWriter writer)
    {
        writer.WriteValue(Value, ValueSuffix);
    }
    
    /// <summary>
    /// Converts the specified value to its string NBT value form.
    /// </summary>
    /// <param name="from">The value to convert from.</param>
    /// <returns>The string NBT value form.</returns>
    public static implicit operator SNbtByteValue(sbyte from)
    {
        return new SNbtByteValue(from);
    }

    /// <summary>
    /// Converts the specified string NBT form of value to its value representation.
    /// </summary>
    /// <param name="from">The string NBT value to convert from.</param>
    /// <returns>The value form.</returns>
    public static implicit operator sbyte(SNbtByteValue from)
    {
        return from.Value;
    }
}