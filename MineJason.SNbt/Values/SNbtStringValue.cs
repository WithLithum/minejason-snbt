﻿namespace MineJason.SNbt.Values;

using JetBrains.Annotations;

[PublicAPI]
public record struct SNbtStringValue : ISNbtValue, ISNbtWritable
{
    public SNbtStringValue(string value)
    {
        Value = value;
    }
    
    public string Value { get; set; }
    
    public string ToSNbtString()
    {
        return EscapeString(Value);
    }

    public static string EscapeString(string value, bool singleQuote = true)
    {
        return singleQuote
            ? $"'{value.Replace("'", "\\'")}'"
            : $"\"{value.Replace("\"", "\\\"")}\"";
    }

    public void WriteTo(SNbtWriter writer)
    {
        writer.WriteValue(Value, true);
    }
    
    /// <summary>
    /// Converts the specified value to its string NBT value form.
    /// </summary>
    /// <param name="from">The value to convert from.</param>
    /// <returns>The string NBT value form.</returns>
    public static implicit operator SNbtStringValue(string from)
    {
        return new SNbtStringValue(from);
    }

    /// <summary>
    /// Converts the specified string NBT form of value to its value representation.
    /// </summary>
    /// <param name="from">The string NBT value to convert from.</param>
    /// <returns>The value form.</returns>
    public static implicit operator string(SNbtStringValue from)
    {
        return from.Value;
    }
}