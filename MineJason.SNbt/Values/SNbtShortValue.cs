﻿namespace MineJason.SNbt.Values;

using System.Globalization;
using JetBrains.Annotations;

/// <summary>
/// Represents a String NBT value of a <see cref="short"/>.
/// </summary>
[PublicAPI]
public record struct SNbtShortValue : ISNbtValue, ISNbtWritable
{
    /// <summary>
    /// The suffix of the NBT representation of this value.
    /// </summary>
    public const char ValueSuffix = 's';
    
    public SNbtShortValue(short value)
    {
        Value = value;
    }
    
    public short Value { get; set; }
    
    public string ToSNbtString()
    {
        return $"{Value.ToString(CultureInfo.InvariantCulture)}{ValueSuffix}";
    }

    public void WriteTo(SNbtWriter writer)
    {
        writer.WriteValue(Value, ValueSuffix);
    }
    
    /// <summary>
    /// Converts the specified value to its string NBT value form.
    /// </summary>
    /// <param name="from">The value to convert from.</param>
    /// <returns>The string NBT value form.</returns>
    public static implicit operator SNbtShortValue(short from)
    {
        return new SNbtShortValue(from);
    }

    /// <summary>
    /// Converts the specified string NBT form of value to its value representation.
    /// </summary>
    /// <param name="from">The string NBT value to convert from.</param>
    /// <returns>The value form.</returns>
    public static implicit operator short(SNbtShortValue from)
    {
        return from.Value;
    }
}