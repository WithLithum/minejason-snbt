﻿using System.Collections.ObjectModel;
using JetBrains.Annotations;

namespace MineJason.SNbt.Values;

public class SNbtIntArray : Collection<int>, ISNbtValue, ISNbtWritable
{
    public string ToSNbtString()
    {
        return SNbtArrayHelper.ToSNbtString('I', this);
    }

    public void WriteTo(SNbtWriter writer)
    {
        SNbtArrayHelper.WriteArray('I', this, writer);
    }
}