﻿namespace MineJason.SNbt.Values;

using JetBrains.Annotations;
using System.Globalization;

[PublicAPI]
public record struct SNbtDoubleValue : ISNbtValue, ISNbtWritable
{
    /// <summary>
    /// The suffix of the NBT representation of this value.
    /// </summary>
    public const char ValueSuffix = 'd';
    
    public SNbtDoubleValue(double value)
    {
        Value = value;
    }
    
    public double Value { get; set; }
    
    public string ToSNbtString()
    {
        return $"{Value.ToString(CultureInfo.InvariantCulture)}{ValueSuffix}";
    }

    public void WriteTo(SNbtWriter writer)
    {
        writer.WriteValue(Value, ValueSuffix);
    }
    
    /// <summary>
    /// Converts the specified value to its string NBT value form.
    /// </summary>
    /// <param name="from">The value to convert from.</param>
    /// <returns>The string NBT value form.</returns>
    public static implicit operator SNbtDoubleValue(double from)
    {
        return new SNbtDoubleValue(from);
    }

    /// <summary>
    /// Converts the specified string NBT form of value to its value representation.
    /// </summary>
    /// <param name="from">The string NBT value to convert from.</param>
    /// <returns>The value form.</returns>
    public static implicit operator double(SNbtDoubleValue from)
    {
        return from.Value;
    }
}