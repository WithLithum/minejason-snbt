﻿using System.Collections;
using System.Globalization;
using System.Text;

namespace MineJason.SNbt.Values;

internal static class SNbtArrayHelper
{
    public static void WriteArray<T>(char specifier, IEnumerable<T> enumerable, SNbtWriter writer)
        where T : IFormattable
    {
        writer.WriteBeginArray(specifier);
        
        foreach (var value in enumerable)
        {
            writer.WriteComma();
            writer.WriteValue(value);
        }
        
        writer.WriteEndList();
    }
    
    public static string ToSNbtString<T>(char specifier, IEnumerable<T> enumerable)
        where T : IFormattable
    {
        var stream = new StringWriter();
        using (var writer = new SNbtWriter(stream))
        {
            WriteArray(specifier, enumerable, writer);
        }

        return stream.ToString();
    }
}