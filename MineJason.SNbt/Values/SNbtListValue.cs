﻿using System.Text;
using JetBrains.Annotations;

namespace MineJason.SNbt.Values;

using System.Collections.ObjectModel;

/// <summary>
/// Represents a value of a list of String NBT values of the same type.
/// </summary>
/// <typeparam name="T">The string NBT value type.</typeparam>
[PublicAPI]
public sealed class SNbtListValue<T> : Collection<T>, ISNbtValue, ISNbtWritable
    where T : ISNbtValue
{
    public string ToSNbtString()
    {
        var stream = new StringWriter();
        using (var writer = new SNbtWriter(stream))
        {
            WriteTo(writer);
        }

        return stream.ToString();
    }

    public void WriteTo(SNbtWriter writer)
    {
        writer.WriteBeginList();
        
        foreach (var value in this)
        {
            writer.WriteComma();
            
            if (value is ISNbtWritable writable)
            {
                writer.WriteValue(writable);
                continue;
            }
            
            writer.WriteValue(value);
        }
        
        writer.WriteEndList();
    }
}