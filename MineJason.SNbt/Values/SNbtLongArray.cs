﻿using System.Collections.ObjectModel;
using JetBrains.Annotations;

namespace MineJason.SNbt.Values;

public sealed class SNbtLongArray : Collection<long>, ISNbtValue, ISNbtWritable
{
    public string ToSNbtString()
    {
        return SNbtArrayHelper.ToSNbtString('L', this);
    }

    public void WriteTo(SNbtWriter writer)
    {
        SNbtArrayHelper.WriteArray('L', this, writer);
    }
}