﻿using JetBrains.Annotations;
using MineJason.SNbt.Values;

namespace MineJason.SNbt;

/// <summary>
/// Provides fluent-syntax building for string NBT compounds.
/// </summary>
[PublicAPI]
public class SNbtObjectBuilder : ISNbtValue, ISNbtWritable
{
    private readonly SNbtCompound _compound = new();

    public SNbtObjectBuilder Property(string key, ISNbtValue value)
    {
        _compound.Add(key, value);
        return this;
    }

    public string ToSNbtString()
    {
        return _compound.ToSNbtString();
    }

    public void WriteTo(SNbtWriter writer)
    {
        _compound.WriteTo(writer);
    }
}