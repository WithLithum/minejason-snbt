# Changelog

This document records all changes made to this project in each version
release.

## [0.2.2-alpha] - 2024/3/22

### Fixed

- Fixed the constructor of `SNbtByteValue` representing `false` as `1` and `true` as `0`.
- Fixed the `BooleanValue` method of `SNbtByteValue` not operating correctly.

## [0.2.1-alpha] - 2024/3/17

### Fixed

- Fixed an issue resulted in adding a `Byte` value into `SNbtByteArray` causing stack overflow.

## [0.2.0-alpha] - 2024/3/17

### Added

- Added `SNbtObjectBuilder`.
- Added `SNbtWriter`.
  - Supports easier writing string NBT components, lists and arrays to any `TextWriter`.
- Added interface `ISNbtWritable` for NBT values with native `SNbtWriter` support.
- Added support for implicit conversion between `String`, `Int`, `Byte`, `Long`, `Short`, `Double` and `Float` types of NBT value and their value representation.

### Changed

- `ToSNbtString()` method of default `ISNbtValue` implementations now internally use `SNbtWriter`.

## [0.1.0-alpha] - 2024/3/15

- Initial release.

[Unreleased]: https://gitlab.com/WithLithum/minejason-snbt/-/compare/v0.2.2-alpha...HEAD
[0.2.2-alpha]: https://gitlab.com/WithLithum/minejason-snbt/-/compare/v0.2.1-alpha...v0.2.2-alpha
[0.2.1-alpha]: https://gitlab.com/WithLithum/minejason-snbt/-/compare/v0.2.0-alpha...v0.2.1-alpha
[0.2.0-alpha]: https://gitlab.com/WithLithum/minejason-snbt/-/compare/v0.1.0-alpha...v0.2.0-alpha
[0.1.0-alpha]: https://gitlab.com/WithLithum/minejason-snbt/tags/v0.1.0-alpha
